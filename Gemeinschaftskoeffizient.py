# Коэффициент Жаккара


def distance_de_jacquard(first_basket, second_basket):
    a = b = c = 0
    for i in range(0, len(first_basket)):
        if first_basket[i] == second_basket[i] and 0 != first_basket[i]:
            c += 1
        if 0 != first_basket[i]:
            a += 1
        if 0 != second_basket[i]:
            b += 1
    a = float(a)
    b = float(b)
    c = float(c)
    if 0 == (a + b - c):
        return 1
    return 1 - (c / (a + b - c))

import numpy

from pyclustering.container.kdtree import kdtree

from Gemeinschaftskoeffizient import *

class CureCluster:

    def __init__(self, point, index):
        self.points = []
        self.indexes = -1

        self.mean = None
        self.rep = []

        if point is not None:
            self.points = [point]
            self.indexes = [index]
            self.mean = point
            self.rep = [point]

        self.closest = None

        self.distance = float('inf')

    def __repr__(self):
        return "%s, %s" % (self.distance, self.points)


class Cure:

    def __init__(self, data, number_cluster):

        self.__pointer_data = self.__prepare_data_points(data)

        self.__clusters = None
        self.__representors = None
        self.__means = None

        self.__number_cluster = number_cluster
        self.__number_represent_points = 5
        self.__compression = 0.5

    def process(self):
        self.__create_queue()  # queue
        self.__create_kdtree()  # create k-d tree

        while len(self.__queue) > self.__number_cluster:
            # print(len(self.__queue))
            # print('\n')
            cluster1 = self.__queue[0]  # cluster that has nearest neighbor.
            cluster2 = cluster1.closest  # closest cluster.

            self.__queue.remove(cluster1)
            self.__queue.remove(cluster2)

            self.__delete_represented_points(cluster1)
            self.__delete_represented_points(cluster2)

            merged_cluster = self.__merge_clusters(cluster1, cluster2)

            self.__insert_represented_points(merged_cluster)

            # Pointers to clusters that should be relocated is stored here.
            cluster_relocation_requests = []

            # Check for the last cluster
            if len(self.__queue) > 0:
                merged_cluster.closest = self.__queue[0]  # arbitrary cluster from queue
                merged_cluster.distance = self.__cluster_distance(merged_cluster, merged_cluster.closest)

                for item in self.__queue:
                    distance = self.__cluster_distance(merged_cluster, item)
                    # Check if distance between new cluster and current is the best than now.
                    if distance < merged_cluster.distance:
                        merged_cluster.closest = item
                        merged_cluster.distance = distance

                    # Check if current cluster has removed neighbor.
                    if (item.closest is cluster1) or (item.closest is cluster2):
                        if item.distance < distance:
                            (item.closest, item.distance) = self.__closest_cluster(item, distance)

                            if item.closest is None:
                                item.closest = merged_cluster
                                item.distance = distance

                        else:
                            item.closest = merged_cluster
                            item.distance = distance

                        cluster_relocation_requests.append(item)

            # New cluster and updated clusters should relocated in queue
            self.__insert_cluster(merged_cluster)
            for item in cluster_relocation_requests:
                self.__relocate_cluster(item)

        # Change cluster representation
        self.__clusters = [cure_cluster_unit.indexes for cure_cluster_unit in self.__queue]
        self.__representors = [cure_cluster_unit.rep for cure_cluster_unit in self.__queue]
        self.__means = [cure_cluster_unit.mean for cure_cluster_unit in self.__queue]

    def get_clusters(self):

        return self.__clusters

    def __prepare_data_points(self, sample):

        if isinstance(sample, numpy.ndarray):
            return sample.tolist()

        return sample

    def __insert_cluster(self, cluster):

        for index in range(len(self.__queue)):
            if cluster.distance < self.__queue[index].distance:
                self.__queue.insert(index, cluster)
                return

        self.__queue.append(cluster)

    def __relocate_cluster(self, cluster):

        self.__queue.remove(cluster)
        self.__insert_cluster(cluster)

    def __closest_cluster(self, cluster, distance):

        nearest_cluster = None
        nearest_distance = float('inf')

        real_euclidean_distance = distance ** 0.5

        for point in cluster.rep:
            # Nearest nodes should be returned (at least it will return itself).
            nearest_nodes = self.__tree.find_nearest_dist_nodes(point, real_euclidean_distance)
            for (candidate_distance, kdtree_node) in nearest_nodes:
                if (candidate_distance < nearest_distance) and (kdtree_node is not None) and (
                        kdtree_node.payload is not cluster):
                    nearest_distance = candidate_distance
                    nearest_cluster = kdtree_node.payload

        return (nearest_cluster, nearest_distance)

    def __insert_represented_points(self, cluster):

        for point in cluster.rep:
            self.__tree.insert(point, cluster)

    def __delete_represented_points(self, cluster):

        for point in cluster.rep:
            self.__tree.remove(point, payload=cluster)

    def __merge_clusters(self, cluster1, cluster2):

        merged_cluster = CureCluster(None, None)

        merged_cluster.points = cluster1.points + cluster2.points
        merged_cluster.indexes = cluster1.indexes + cluster2.indexes

        dimension = len(cluster1.mean)
        merged_cluster.mean = [0] * dimension
        if merged_cluster.points[1:] == merged_cluster.points[:-1]:
            merged_cluster.mean = merged_cluster.points[0]
        else:
            for index in range(dimension):
                merged_cluster.mean[index] = (len(cluster1.points) * cluster1.mean[index] + len(cluster2.points) *
                                              cluster2.mean[index]) / (len(cluster1.points) + len(cluster2.points));

        temporary = list()

        for index in range(self.__number_represent_points):
            maximal_distance = 0
            maximal_point = None

            for point in merged_cluster.points:
                minimal_distance = 0
                if index == 0:
                    minimal_distance = distance_de_jacquard(point, merged_cluster.mean)
                else:
                    minimal_distance = min([distance_de_jacquard(point, p) for p in temporary])

                if minimal_distance >= maximal_distance:
                    maximal_distance = minimal_distance
                    maximal_point = point

            if maximal_point not in temporary:
                temporary.append(maximal_point)

        for point in temporary:
            representative_point = [0] * dimension
            for index in range(dimension):
                representative_point[index] = point[index] + self.__compression * (
                        merged_cluster.mean[index] - point[index])

            merged_cluster.rep.append(representative_point)

        return merged_cluster

    def __create_queue(self):

        self.__queue = [CureCluster(self.__pointer_data[index_point], index_point) for index_point in
                        range(len(self.__pointer_data))]

        # set closest clusters
        for i in range(0, len(self.__queue)):
            minimal_distance = float('inf')
            closest_index_cluster = -1

            for k in range(0, len(self.__queue)):
                if i != k:
                    dist = self.__cluster_distance(self.__queue[i], self.__queue[k])
                    if dist < minimal_distance:
                        minimal_distance = dist
                        closest_index_cluster = k

            self.__queue[i].closest = self.__queue[closest_index_cluster]
            self.__queue[i].distance = minimal_distance

        self.__queue.sort(key=lambda x: x.distance, reverse=False)

    def __create_kdtree(self):

        self.__tree = kdtree()
        for current_cluster in self.__queue:
            for representative_point in current_cluster.rep:
                self.__tree.insert(representative_point, current_cluster)

    def __cluster_distance(self, cluster1, cluster2):

        distance = float('inf')
        for i in range(0, len(cluster1.rep)):
            for k in range(0, len(cluster2.rep)):
                dist = distance_de_jacquard(cluster1.rep[i], cluster2.rep[k])
                if dist < distance:
                    distance = dist

        return distance

from cure import *
import os
import time

t0 = time.time()
baskets = []
with open("baskets.txt", "r") as file:
    lines = file.readlines()
    for line in lines:
        products = line.strip().split(",")
        products = map(int, products)
        products = list(products)
        baskets.append(products)

cure_object = Cure(baskets, 3)
cure_object.process()
result = cure_object.get_clusters()

# os.system("rm -r output")
# os.system("mkdir output")
for i in range(len(result)):
    f = open("output/cluster_" + str(i) + ".txt", "w+")
    for basket in range(len(result[i])):
        f.write(str(baskets[result[i][basket]]))
        f.write('\n')
    f.close()
t1 = time.time()
print(t1 - t0)
